<x-guest-layout>
    <x-auth-card>

        <!-- Session Status -->
        <x-auth-session-status class="mb-4" :status="session('status')" />

        <!-- Validation Errors -->
        <x-backend.layouts.elements.errors class="mb-4" :errors="$errors" />

        <form method="POST" action="{{ route('login') }}">
            @csrf

            <!-- Email Address -->
            <div>
                <x-backend.form.input name="email" :value="old('email')"/>
            </div>

            <!-- Password -->
            <div class="mt-4">
                <x-backend.form.input name="password" type="password" :value="old('password')"/>
            </div>

            <!-- Remember Me -->
            <div class="block mt-4">
                <x-backend.form.checkbox name="checkbox" label="Remember All"/>
            </div>

            <div class="flex items-center justify-end mt-4">
                @if (Route::has('password.request'))
                    <a class="btn btn-sm btn-primary" href="{{ route('password.request') }}" role="button">
                        {{ __('Forgot your password?') }}
                    </a>
                @endif
                <x-backend.form.button>Log in</x-backend.form.button>
            </div>
        </form>
    </x-auth-card>
</x-guest-layout>
