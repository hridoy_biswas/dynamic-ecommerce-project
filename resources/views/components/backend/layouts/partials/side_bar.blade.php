<div id="layoutSidenav_nav">
    <nav class="sb-sidenav accordion sb-sidenav-dark" id="sidenavAccordion">
        <div class="sb-sidenav-menu">
            <div class="nav">
                <div class="sb-sidenav-menu-heading">Core</div>
                <a class="nav-link" href="{{ Route('dashboard') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-tachometer-alt"></i></div>
                    Dashboard
                </a>

                @can('admin') 
                <div class="sb-sidenav-menu-heading">Interface</div>
                <a class="nav-link collapsed" href="#" data-bs-toggle="collapse" data-bs-target="#collapseLayouts" aria-expanded="false" aria-controls="collapseLayouts">
                    <div class="sb-nav-link-icon"><i class="fas fa-columns"></i></div>
                    Users Management
                    <div class="sb-sidenav-collapse-arrow"><i class="fas fa-angle-down"></i></div>
                </a>
                <div class="collapse" id="collapseLayouts" aria-labelledby="headingOne" data-bs-parent="#sidenavAccordion">
                    <nav class="sb-sidenav-menu-nested nav">
                        <a class="nav-link" href="{{ route('roles.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            Roles
                        </a>
                        <a class="nav-link" href="{{ route('users.index') }}">
                            <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                            Users
                        </a>
                    </nav>
                </div>
                @endcan
 
                <div class="sb-sidenav-menu-heading">Addons</div>
                <a class="nav-link" href="{{ route('orders.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Orders
                </a>
                <a class="nav-link" href="{{ route('categories.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Categories
                </a>
                <a class="nav-link" href="{{ route('sub-categories.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Sub Categories
                </a>
                <a class="nav-link" href="{{ route('products.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    products
                </a>
                <a class="nav-link" href="{{ route('tags.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Tags
                </a>
                <a class="nav-link" href="{{ route('sizes.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Sizes
                </a>
                <a class="nav-link" href="{{ route('colors.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Colors
                </a>
                <a class="nav-link" href="{{ route('brands.index') }}">
                    <div class="sb-nav-link-icon"><i class="fas fa-table"></i></div>
                    Brands
                </a>
            </div>
        </div>
        <div class="sb-sidenav-footer">
            <div class="small">Logged in as:</div>
            {{ auth()->user()->role->name ?? '' }}
        </div>
    </nav>
</div>