<h6 class="mb-2">Free Shipping</h6>
<p class="mb-0">We deliver to over 100 countries around the world. For full details of
    the delivery options we offer, please view our <a href="#"
        class="text-primary">Delivery
        information</a> We hope you’ll love every
    purchase, but if you ever need to return an item you can do so within a month of
    receipt. For full details of how to make a return, please view our <a href="#"
        class="text-primary">Returns information</a>
</p>