<div class="row mt-6">
    <div class="col-md-6">
        <h5 class="description-title mb-4 font-weight-semi-bold ls-m">Features</h5>
        <p class="mb-2">
            {!! $product->long_description ?? '' !!}
        </p>
    </div>
</div>