<!DOCTYPE html>
<html lang="en">

<head>
    <meta charset="UTF-8">
    <meta name="csrf-token" content="{{ csrf_token() }}">
    <meta name="viewport" content="width=device-width, initial-scale=1.0, minimum-scale=1.0">
    <title>{{ $pagetitle ?? 'Home' }}</title>

    <meta name="keywords" content="HTML5 Template" />
    <meta name="description" content="Riode - Ultimate eCommerce Template">
    <meta name="author" content="D-THEMES">

    <!-- Favicon -->
    <link rel="icon" type="image/png" href="{{ asset('ui/frontend/images/icons/favicon.png') }}">
    <!-- Preload Font -->
    <link rel="preload" href="{{ asset('ui/frontend/fonts/riode.ttf?5gap68" as="font" type="font/woff2" crossorigin="anonymous') }}">
    <link rel="preload" href="{{ asset('ui/frontend/vendor/fontawesome-free/webfonts/fa-solid-900.woff2" as="font" type="font/woff2"
        crossorigin="anonymous') }}">
    <link rel="preload" href="{{ asset('ui/frontend/vendor/fontawesome-free/webfonts/fa-brands-400.woff2" as="font" type="font/woff2" crossorigin="anonymous') }}">
    <script>
        WebFontConfig = {
            google: { families: [ 'Poppins:300,400,500,600,700,800' ] }
        };
        ( function ( d ) {
            var wf = d.createElement( 'script' ), s = d.scripts[ 0 ];
            wf.src = '{{ asset('ui/frontend/js/webfont.js') }}';
            wf.async = true;
            s.parentNode.insertBefore( wf, s );
        } )( document );
    </script>

    <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/vendor/fontawesome-free/css/all.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/vendor/animate/animate.min.css') }}">

    <!-- Font Awesome Icon Library -->
<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">

    <!-- Plugins CSS File -->
    <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/vendor/magnific-popup/magnific-popup.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/vendor/owl-carousel/owl.carousel.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/vendor/nouislider/nouislider.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/vendor/sticky-icon/stickyicon.css') }}">

    <!--Bootstrap css-->
    <link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.1.3/css/bootstrap.min.css" integrity="sha384-MCw98/SFnGE8fJT3GXwEOngsV7Zt27NXFoaoApmYm81iuXoPkFOJwJ8ERdknLPMO" crossorigin="anonymous">

    <!-- Main CSS File -->
    <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/css/demo2.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/css/style.min.css') }}">
    <link rel="stylesheet" type="text/css" href="{{ asset('ui/frontend/css/style.css') }}">

    
    
</head>

<body class="home">
    <!-- Start Header -->
    <div class="page-wrapper">
        <h1 class="d-none">Riode - Responsive eCommerce HTML Template</h1>
        <header class="header">
            
            <!-- Start Header center -->
            <x-frontend.layouts.partials.header/>
            <!-- End Header center -->

            <!-- Start Header bottom -->
            <x-frontend.layouts.partials.headerlink/>
            {{-- :categories="$categories" --}}
            <!-- End Header bottom -->
            
        </header>
        <!-- End Header -->
        <main class="main">
            
            {{ $slot }}

        </main>
        <!-- End of Main -->

        <!-- Start of Footer -->
            <x-frontend.layouts.partials.footer/>
        <!-- End of Footer -->
    </div>

    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/popper.js@1.12.9/dist/umd/popper.min.js" integrity="sha384-ApNbgh9B+Y1QKtv3Rn7W3mgPxhU9K/ScQsAP7hUibX39j7fakFPskvXusvfa0b4Q" crossorigin="anonymous"></script>
    <script src="https://cdn.jsdelivr.net/npm/bootstrap@4.0.0/dist/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>


	<!-- Plugins JS File -->
    <script src="{{ asset('ui/frontend/vendor/sticky/sticky.min.js') }}"></script>
    <script src="{{ asset('ui/frontend/vendor/imagesloaded/imagesloaded.pkgd.min.js') }}"></script>
    <script src="{{ asset('ui/frontend/vendor/elevatezoom/jquery.elevatezoom.min.js') }}"></script>
    <script src="{{ asset('ui/frontend/vendor/magnific-popup/jquery.magnific-popup.min.js') }}"></script>
    <script src="{{ asset('ui/frontend/vendor/isotope/isotope.pkgd.min.js') }}"></script>
    <script src="{{ asset('ui/frontend/vendor/skrollr/skrollr.min.js') }}"></script>
    <script src="{{ asset('ui/frontend/vendor/nouislider/nouislider.min.js') }}"></script>
    <script src="{{ asset('ui/frontend/vendor/owl-carousel/owl.carousel.min.js') }}"></script>
    <script src="{{ asset('ui/frontend/vendor/jquery/jquery.min.js') }}"></script>
    <!-- Main JS File -->
    <script src="{{ asset('ui/frontend/js/main.min.js') }}"></script>

    <script src="https://unpkg.com/sweetalert/dist/sweetalert.min.js"></script>
    <script src="{{ asset('js/custom.js') }}"></script>
    @if (session('status'))
        <script>
            swal("{{ session('status') }}");
        </script>
    @endif
</body>

</html>