<div class="row">
    <div class="col-lg-4 mb-6">
        <div class="avg-rating-container">
            <mark>5.0</mark>
            <div class="avg-rating">
                <span class="avg-rating-title">Average Rating</span>
                <div class="ratings-container mb-0">
                    <div class="ratings-full">
                        <span class="ratings" style="width:100%"></span>
                        <span class="tooltiptext tooltip-top"></span>
                    </div>
                    <span class="rating-reviews">(2 Reviews)</span>
                </div>
            </div>
        </div>
        <div class="ratings-list mb-2">
            <div class="ratings-item">
                <div class="ratings-container mb-0">
                    <div class="ratings-full">
                        <span class="ratings" style="width:100%"></span>
                        <span class="tooltiptext tooltip-top"></span>
                    </div>
                </div>
                <div class="rating-percent">
                    <span style="width:100%;"></span>
                </div>
                <div class="progress-value">100%</div>
            </div>
            <div class="ratings-item">
                <div class="ratings-container mb-0">
                    <div class="ratings-full">
                        <span class="ratings" style="width:80%"></span>
                        <span class="tooltiptext tooltip-top">4.00</span>
                    </div>
                </div>
                <div class="rating-percent">
                    <span style="width:0%;"></span>
                </div>
                <div class="progress-value">0%</div>
            </div>
            <div class="ratings-item">
                <div class="ratings-container mb-0">
                    <div class="ratings-full">
                        <span class="ratings" style="width:60%"></span>
                        <span class="tooltiptext tooltip-top">4.00</span>
                    </div>
                </div>
                <div class="rating-percent">
                    <span style="width:0%;"></span>
                </div>
                <div class="progress-value">0%</div>
            </div>
            <div class="ratings-item">
                <div class="ratings-container mb-0">
                    <div class="ratings-full">
                        <span class="ratings" style="width:40%"></span>
                        <span class="tooltiptext tooltip-top">4.00</span>
                    </div>
                </div>
                <div class="rating-percent">
                    <span style="width:0%;"></span>
                </div>
                <div class="progress-value">0%</div>
            </div>
            <div class="ratings-item">
                <div class="ratings-container mb-0">
                    <div class="ratings-full">
                        <span class="ratings" style="width:20%"></span>
                        <span class="tooltiptext tooltip-top">4.00</span>
                    </div>
                </div>
                <div class="rating-percent">
                    <span style="width:0%;"></span>
                </div>
                <div class="progress-value">0%</div>
            </div>
        </div>
        <a class="btn btn-dark btn-rounded submit-review-toggle" href="#">Submit
            Review</a>
    </div>
    <div class="col-lg-8 comments pt-2 pb-10 border-no">
        <nav class="toolbox">
            <div class="toolbox-left">
                <div class="toolbox-item">
                    <a href="#" class="btn btn-outline btn-rounded">All Reviews</a>
                </div>
                <div class="toolbox-item">
                    <a href="#" class="btn btn-outline btn-rounded">With Images</a>
                </div>
                <div class="toolbox-item">
                    <a href="#" class="btn btn-outline btn-rounded">With Videos</a>
                </div>
            </div>
            <div class="toolbox-right">
                <div class="toolbox-item toolbox-sort select-box text-dark">
                    <label>Sort By :</label>
                    <select name="orderby" class="form-control">
                        <option value="">Default Order</option>
                        <option value="newest" selected="selected">Newest Reviews
                        </option>
                        <option value="oldest">Oldest Reviews</option>
                        <option value="high_rate">Highest Rating</option>
                        <option value="low_rate">Lowest Rating</option>
                        <option value="most_likely">Most Likely</option>
                        <option value="most_unlikely">Most Unlikely</option>
                    </select>
                </div>
            </div>
        </nav>
        
        <ul class="comments-list">
            {{-- @dd($comments->all()) --}}
            @foreach ($comments as $comment)
            <li>
                <div class="comment">
                    <figure class="comment-media">
                        <a href="#">
                            <img src="{{ asset('storage/images/profiles/'.$comment->createdBy->image) }}" alt="avatar">
                        </a>
                    </figure>
                    <div class="comment-body">
                        {{-- <div class="comment-rating ratings-container">
                            <div class="ratings-full">
                                <span class="ratings" style="width:100%"></span>
                                <span class="tooltiptext tooltip-top"></span>
                            </div>
                        </div> --}}
                        <div class="comment-user">
                            <span class="comment-date">by <span
                                    class="font-weight-semi-bold text-uppercase text-dark">{{ $comment->createdBy->name ?? 'Anonymous' }}</span> on
                                <span class="font-weight-semi-bold text-dark">{{ $comment->created_at->diffForHumans() }}</span></span>
                        </div>
                        <div class="comment-content">
                            <p>{{ $comment->body }}</p>
                        </div>
                        {{-- <div class="feeling mt-5">
                            <button
                                class="btn btn-link btn-icon-left btn-slide-up btn-infinite like mr-2">
                                <i class="fa fa-thumbs-up"></i>
                                Like (<span class="count">0</span>)
                            </button>
                        </div> --}}
                        
                    </div>

                </div>
            </li>
            @endforeach

          

            @auth
            <form action="{{ route('comment.store',['id' => $product->id]) }}" method="POST">
                @csrf
                <div class="rating-css">
                    <div class="star-icon">
                        <input type="radio" value="1" name="product_rating" checked id="rating1">
                        <label for="rating1" class="fa fa-star"></label>
                        <input type="radio" value="2" name="product_rating" id="rating2">
                        <label for="rating2" class="fa fa-star"></label>
                        <input type="radio" value="3" name="product_rating" id="rating3">
                        <label for="rating3" class="fa fa-star"></label>
                        <input type="radio" value="4" name="product_rating" id="rating4">
                        <label for="rating4" class="fa fa-star"></label>
                        <input type="radio" value="5" name="product_rating" id="rating5">
                        <label for="rating5" class="fa fa-star"></label>
                    </div>
                </div>
                <x-frontend.form.textarea name="comment">
                </x-frontend.form.textarea>
                <button type="submit">
                    Save
                </button>
            </form>
            @endauth
            
        </ul>
        <nav class="toolbox toolbox-pagination justify-content-end">
            {{ $comments->links() }}
        </nav>
    </div>
</div>
<!-- End Comments -->
<div class="review-form-section">
    <div class="review-overlay"></div>
    <div class="review-form-wrapper">
        <div class="title-wrapper text-left">
            <h3 class="title title-simple text-left text-normal">Add a Review</h3>
            <p>Your email address will not be published. Required fields are marked *
            </p>
        </div>
        <div class="rating-form">
            <label for="rating" class="text-dark">Your rating * </label>
            <span class="rating-stars selected">
                <a class="star-1" href="#">1</a>
                <a class="star-2" href="#">2</a>
                <a class="star-3" href="#">3</a>
                <a class="star-4 active" href="#">4</a>
                <a class="star-5" href="#">5</a>
            </span>

            <select name="rating" id="rating" required="" style="display: none;">
                <option value="">Rate…</option>
                <option value="5">Perfect</option>
                <option value="4">Good</option>
                <option value="3">Average</option>
                <option value="2">Not that bad</option>
                <option value="1">Very poor</option>
            </select>
        </div>
        <form action="#">
            <textarea id="reply-message" cols="30" rows="6" class="form-control mb-4"
                placeholder="Comment *" required></textarea>

            <div class="review-medias">
                <div class="file-input form-control image-input"
                    style="background-image: url(images/product/placeholder.png);">
                    <div class="file-input-wrapper">
                    </div>
                    <label class="btn-action btn-upload" title="Upload Media">
                        <input type="file" accept=".png, .jpg, .jpeg"
                            name="riode_comment_medias_image_1">
                    </label>
                    <label class="btn-action btn-remove" title="Remove Media">
                    </label>
                </div>
                <div class="file-input form-control image-input"
                    style="background-image: url(images/product/placeholder.png);">
                    <div class="file-input-wrapper">
                    </div>
                    <label class=" btn-action btn-upload" title="Upload Media">
                        <input type="file" accept=".png, .jpg, .jpeg"
                            name="riode_comment_medias_image_2">
                    </label>
                    <label class="btn-action btn-remove" title="Remove Media">
                    </label>
                </div>
                <div class="file-input form-control video-input"
                    style="background-image: url(images/product/placeholder.png);">
                    <video class="file-input-wrapper" controls=""></video>
                    <label class="btn-action btn-upload" title="Upload Media">
                        <input type="file" accept=".avi, .mp4"
                            name="riode_comment_medias_video_1">
                    </label>
                    <label class="btn-action btn-remove" title="Remove Media">
                    </label>
                </div>
            </div>
            <p>Upload images and videos. Maximum count: 3, size: 2MB</p>
            <button type="submit" class="btn btn-primary btn-rounded">Submit<i
                    class="d-icon-arrow-right"></i></button>
        </form>
    </div>
</div>
<!-- End Reply -->