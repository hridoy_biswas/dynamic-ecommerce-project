<h3 class="widget-title">Color</h3>
<ul class="widget-body filter-items">
    @foreach ($colors as $key=>$value)
    <li><a href="#">{{ $value }}</a></li>
    @endforeach
</ul>