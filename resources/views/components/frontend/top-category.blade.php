<div class="row grid">
    <div class="grid-item col-md-6 height-x2">
        <div class="banner banner-fixed content-middle content-center overlay-dark">
            <a href="demo2-shop.html">
                <figure>
                    <img src="{{ asset('ui/frontend/images/demos/demo2/category/1.jpg') }}" alt="category" width="585"
                        height="397" style="background-color: #c8ced4;" />
                </figure>
            </a>
            <div class="banner-content text-center w-100 h-100 d-flex flex-column">
                <h3 class="banner-title text-white text-uppercase ls-s mb-0">Padded Clothes</h3>
                <h4
                    class="banner-subtitle flex-1 font-weight-normal text-capitalize text-body ls-md lh-1 mb-0">
                    Collection</h4>
                <div class="btn-group d-flex justify-content-center">
                    <a href="demo2-shop.html"
                        class="btn btn-white btn-rounded font-weight-semi-bold">Women’s</a>
                    <a href="demo2-shop.html"
                        class="btn btn-white btn-rounded font-weight-semi-bold">Men's</a>
                </div>
            </div>
        </div>
    </div>
    <div class="grid-item col-sm-6 height-x1">
        <div class="category category-light category-absolute overlay-dark">
            <a href="demo2-shop.html">
                <figure class="category-media">
                    <img src="{{ asset('ui/frontend/images/demos/demo2/category/2.png') }}" alt="category" width="585"
                        height="205" style="background-color: #c8ced4;" />
                </figure>
            </a>
            <div class="category-content">
                <h4 class="category-name"><a href="demo2-shop.html">Women’s T-Shirt</a></h4>
            </div>
        </div>
        <div class="category category-light category-absolute overlay-dark mt-4">
            <a href="demo2-shop.html">
                <figure class="category-media">
                    <img src="{{ asset('ui/frontend/images/demos/demo2/category/3.jpg') }}" alt="category" width="585"
                        height="205" style="background-color: #ebedef;" />
                </figure>
            </a>
            <div class="category-content">
                <h4 class="category-name"><a href="demo2-shop.html">Sports & Outdoors</a></h4>
            </div>
        </div>
    </div>
    
</div>