@props(['categories'])
<h3 class="widget-title">All Categories</h3>
<ul class="widget-body filter-items search-ul">
    @foreach ($categories as $key=>$value)
    <li><a href="#">{{ $value }}</a></li>
    @endforeach
</ul>