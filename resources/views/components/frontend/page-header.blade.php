<h3 class="page-subtitle">Categories</h3>
<h1 class="page-title">Classic Filter</h1>
<ul class="breadcrumb">
    <li><a href="{{ route('home') }}"><i class="d-icon-home"></i></a></li>
    <li class="delimiter">/</li>
    <li><a href="shop.html">Categories</a></li>
    <li class="delimiter">/</li>
    <li>Classic Filter</li>
</ul>