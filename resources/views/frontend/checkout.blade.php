<x-frontend.layouts.master>

<hr>
<div class="page-content pt-7 pb-10 mb-10">
    <div class="step-by pr-4 pl-4">
        <h3 class="title title-simple title-step">1. Shopping Cart</a></h3>
        <h3 class="title title-simple title-step active">2. Checkout</a></h3>
    </div>
    <div class="container mt-7">
        <form action="{{ route('placeOrder') }}" method="POST" class="form">
            @csrf
            <div class="row">
                <div class="col-lg-7 mb-6 mb-lg-0 pr-lg-4">
                    <h3 class="title title-simple text-left text-uppercase">Billing Details</h3>
                    <div class="row mb-2">
                        <div class="form-check col-md-6">
                            <input class="form-check-input" type="radio" name="delivery" id="home" value="home" checked>
                            <label class="form-check-label" for="home">
                              Home
                            </label>
                          </div>
                          <div class="form-check col-md-6">
                            <input class="form-check-input" type="radio" name="delivery" id="office" value="office">
                            <label class="form-check-label" for="office">
                              Office
                            </label>
                          </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <label>Name</label>
                            <input type="text" class="form-control" value="{{ Auth::user()->name }}" name="name" required="" />
                        </div>
                        
                        <div class="col-md-12">
                            <label>Email Address</label>
                            <input type="email" class="form-control" value="{{ Auth::user()->email }}" name="email" required="" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-12">
                            <label>Phone</label>
                            <input type="number" class="form-control" value="{{ Auth::user()->profile->mobile_number }}" name="phone" required="" />
                        </div>
                    </div>
                    <div class="row">
                        <div class="col-xs-6">
                            <label for="city">City</label>
                            <select id="city" class="form-control" name="city">
                                <option value="">Choose...</option>
                                <option {{ Auth::user()->profile->city == 'dhaka' ? 'selected' : '' }} value="dhaka">Dhaka</option>
                                <option {{ Auth::user()->profile->city == 'barisal' ? 'selected' : '' }} value="barisal">Barisal</option>
                                <option {{ Auth::user()->profile->city == 'khulna' ? 'selected' : '' }} value="khulna">Khulna</option>
                            </select>
                        </div>
                        <div class="col-xs-6">
                            <label for="area">Area</label>
                            <select id="area" class="form-control" name="area">
                                <option value="">Choose...</option>
                                <option {{ Auth::user()->profile->area == 'jhalokati' ? 'selected' : '' }} value="jhalokati">Jhalokati</option>
                                <option {{ Auth::user()->profile->area == 'nalcity' ? 'selected' : '' }} value="nalcity">Nalcity</option>
                                <option {{ Auth::user()->profile->area == 'uttara' ? 'selected' : '' }} value="uttara">Uttara</option>
                            </select>
                        </div>
                        <div class="form-group col-md-6">
                            <label for="country">Country</label>
                            <select id="country" class="form-control" name="country">
                                <option value="">Choose...</option>
                                <option {{ Auth::user()->profile->country == 'bangladesh' ? 'selected' : '' }} value="bangladesh">Bangladesh</option>
                            </select>
                        </div>
                        <div class="col-xs-6">
                            <label>Post Code</label>
                            <input type="number" class="form-control" name="postcode" value="{{ Auth::user()->profile->postcode }}" />
                        </div>
                        <div class="col-md-12">
                            <div class="form-group">
                                <label for="address">Address</label>
                                <textarea class="form-control"  name="address" rows="3">{{ Auth::user()->profile->address }}</textarea>
                              </div>
                        </div>
                    </div>
                    
                </div>
                <aside class="col-lg-5 sticky-sidebar-wrapper">
                    <div class="sticky-sidebar mt-1" data-sticky-options="{'bottom': 50}">
                        <div class="summary pt-5">
                            <h3 class="title title-simple text-left text-uppercase">Your Order</h3>
                            <table class="order-table">
                                <thead>
                                    <tr>
                                        <th>Product</th>
                                        <th>Price</th>
                                    </tr>
                                </thead>
                                <tbody>
                                    @php
                                        $total = 0;
                                    @endphp
                                    @foreach ($cartItem as $item)
                                    <tr>
                                        <td class="product-name">{{ $item->products->name }}<span
                                                class="product-quantity">×&nbsp;{{ $item->prod_qty }}</span></td>
                                        <td class="product-total text-body">TK. {{ $item->products->price }}</td>
                                    </tr>
                                    @php
                                        $total +=$item->products->price * $item->prod_qty
                                    @endphp
                                    @endforeach
                                    <tr class="summary-subtotal">
                                        <td>
                                            <h4 class="summary-subtitle">Subtotal</h4>
                                        </td>
                                        <td class="summary-subtotal-price pb-0 pt-0">TK {{ $total }}
                                        </td>
                                    </tr>
                                    <tr class="sumnary-shipping shipping-row-last">
                                        <td colspan="2">
                                            <h4 class="summary-subtitle">Calculate Shipping</h4>
                                            <ul>
                                                <li>
                                                    <div class="custom-radio">
                                                        <input type="radio" id="flat_rate" name="shipping" class="custom-control-input" value="flat_rate" checked>
                                                        <label class="custom-control-label" for="flat_rate">Flat rate</label>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="custom-radio">
                                                        <input type="radio" id="free_shipping" name="shipping" class="custom-control-input" value="free_shipping">
                                                        <label class="custom-control-label" for="free_shipping">Free Shipping</label>
                                                    </div>
                                                </li>

                                                <li>
                                                    <div class="custom-radio">
                                                        <input type="radio" id="local_pickup" name="shipping" class="custom-control-input" value="local_pickup">
                                                        <label class="custom-control-label" for="local_pickup">Local pickup</label>
                                                    </div>
                                                </li>
                                            </ul>
                                        </td>
                                    </tr>
                                    <tr class="summary-total">
                                        <td class="pb-0">
                                            <h4 class="summary-subtitle">Total</h4>
                                        </td>
                                        <td class=" pt-0 pb-0">
                                            <p class="summary-total-price ls-s text-primary"> Tk. {{ $total }}</p>
                                        </td>
                                    </tr>
                                </tbody>
                            </table>
                            <div class="payment accordion radio-type">
                                <h4 class="summary-subtitle ls-m pb-3">Payment Methods</h4>
                                        <div class="custom-radio">
                                            <input type="radio" id="cod"
                                                name="payment" class="custom-control-input" value="cash_on_delivery">
                                            <label class="custom-control-label"
                                                for="cod">Cash On Delivery</label>
                                        </div>
                            </div>
                            <div class="form-checkbox mt-4 mb-5">
                                <input type="checkbox" class="custom-checkbox" id="terms-condition"
                                    name="terms-condition" />
                                <label class="form-control-label" for="terms-condition">
                                    I have read and agree to the website <a href="#">terms and conditions
                                    </a>*
                                </label>
                            </div>
                            <button type="submit" class="btn btn-dark btn-rounded btn-order">Place
                                Order</button>
                        </div>
                    </div>
                </aside>
            </div>
        </form>
    </div>
</div>

</x-frontend.layouts.master>