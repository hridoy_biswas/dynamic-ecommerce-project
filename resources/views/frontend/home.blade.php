<x-frontend.layouts.master>
    {{-- :categories="$categories" --}}
    <x-slot name="pagetitle">
        Home
    </x-slot>

    <div class="page-content">
        <div class="container">
            <section class="intro-section">
                <div class="row">

                    <x-frontend.slider/>

                </div>
            </section>

            <section class="pt-3">
                <div class="row mb-2">
                    <div class="col-md-12">
                         <a href="{{ route('categories-show.shop',['category'=>1]) }}">
                            <img src="{{  asset('ui/frontend/images/demos/demo2/banners/SmartPhone.jpg') }}" alt="" srcset="">
                         </a>
                    </div>
                 </div>
                <h2 class="title title-simple text-left ls-m">Mobile</h2>
                <hr>
                <div class="product-wrapper row">
                   @foreach ($mobiles as $product)
                   <div class="col-lg-3 col-md-4 col-6 mb-2">
                       <x-frontend.product-card :product="$product"/>
                   </div>
                   @endforeach
                    
                </div>
            </section>
            
            <section class="pt-3">
                <div class="row mb-2">
                    <div class="col-md-12">
                         <a href="{{ route('categories-show.shop',['category'=>3]) }}">
                            <img src="{{  asset('ui/frontend/images/demos/demo2/banners/Mobile-ac-updated.jpg') }}" alt="" srcset="">
                         </a>
                    </div>
                 </div>
                <h2 class="title title-simple text-left ls-m">Mobile Accessories</h2>
                <hr>
                <div class="product-wrapper row">
                   @foreach ($mobiles_accessories as $product)
                   <div class="col-lg-3 col-md-4 col-6 mb-2">
                       <x-frontend.product-card :product="$product"/>
                   </div>
                   @endforeach
                    
                </div>
            </section>
            
            <section class="pt-3">
                <div class="row mb-2">
                    <div class="col-md-12">
                         <a href="{{ route('sub-categories-show.shop',['sub_category'=>6]) }}">
                            <img src="{{  asset('ui/frontend/images/demos/demo2/banners/Watch-Top-banner-2_1_.jpg') }}" alt="" srcset="">
                         </a>
                    </div>
                 </div>
                <h2 class="title title-simple text-left ls-m">Smart Watch</h2>
                <hr>
                <div class="product-wrapper row">
                   @foreach ($smart_watch as $product)
                   <div class="col-lg-3 col-md-4 col-6 mb-2">
                       <x-frontend.product-card :product="$product"/>
                   </div>
                   @endforeach
                    
                </div>
            </section>
            
            <section class="pt-3">
                <div class="row mb-2">
                    <div class="col-md-12">
                         <a href="{{ route('categories-show.shop',['category'=>4]) }}">
                            <img src="{{  asset('ui/frontend/images/demos/demo2/banners/Computer-ac-updated.jpg') }}" alt="" srcset="">
                         </a>
                    </div>
                 </div>
                <h2 class="title title-simple text-left ls-m">Computer Accesories</h2>
                <hr>
                <div class="product-wrapper row">
                   @foreach ($computer_accesories as $product)
                   <div class="col-lg-3 col-md-4 col-6 mb-2">
                       <x-frontend.product-card :product="$product"/>
                   </div>
                   @endforeach
                    
                </div>
            </section>
            
            <section class="pt-3">
                <div class="row mb-2">
                    <div class="col-md-12">
                         <a href="{{ route('categories-show.shop',['category'=>5]) }}">
                            <img src="{{  asset('ui/frontend/images/demos/demo2/banners/LIFESTYLE.jpg') }}" alt="" srcset="">
                         </a>
                    </div>
                 </div>
                <h2 class="title title-simple text-left ls-m">LifeStyle</h2>
                <hr>
                <div class="product-wrapper row">
                   @foreach ($life_style as $product)
                   <div class="col-lg-3 col-md-4 col-6 mb-2">
                       <x-frontend.product-card :product="$product"/>
                   </div>
                   @endforeach
                    
                </div>
            </section>
            
            <section class="pt-3">
                <div class="row mb-2">
                    <div class="col-md-12">
                         <a href="{{ route('categories-show.shop',['category'=>2]) }}">
                            <img src="{{  asset('ui/frontend/images/demos/demo2/banners/Appliances-updated.jpg') }}" alt="" srcset="">
                         </a>
                    </div>
                 </div>
                <h2 class="title title-simple text-left ls-m">Electronics & Appliances</h2>
                <hr>
                <div class="product-wrapper row">
                   @foreach ($electronics_appliances as $product)
                   <div class="col-lg-3 col-md-4 col-6 mb-2">
                       <x-frontend.product-card :product="$product"/>
                   </div>
                   @endforeach
                    
                </div>
            </section>
          
        </div>
    </div>

</x-frontend.layouts.master>    