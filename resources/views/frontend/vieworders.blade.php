<x-frontend.layouts.master>

    <x-slot name="pagetitle">
        View Order
    </x-slot>

    <div class="page-content">
        <div class="container mt-7 mb-2">
            <div class="cart-actions mb-6 pt-4">
                <a href="#" class="btn btn-md btn-rounded btn-icon-left mr-4 mb-4">View Order</a>
                <a href="{{ route('my-order') }}" class="btn btn-dark btn-md btn-rounded btn-icon-left mr-4 mb-4"><i class="d-icon-arrow-left"></i>Back</a>
            </div>
            <div class="row">
                <div class="col-md-6">
                    <h4>Shipping Details</h4>
                    <hr>
                    <div class="border p-2">
                        <label for="">Name</label>
                        <div class="border p-2">{{ $orders->name }}</div>
                        <label for="">Email</label>
                        <div class="border p-2">{{ $orders->email }}</div>
                        <label for="">Phone</label>
                        <div class="border p-2">{{ $orders->phone }}</div>
                        <label for="">Address</label>
                        <div class="border p-2">{{ $orders->address }}</div>
                        <label for="">City</label>
                        <div class="border p-2">{{ $orders->city }}</div>
                        <label for="">Area</label>
                        <div class="border p-2">{{ $orders->area }}</div>
                        <label for="">Country</label>
                        <div class="border p-2">{{ $orders->country }}</div>
                        <label for="">Post Code</label>
                        <div class="border p-2">{{ $orders->postcode }}</div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h4>Order Details</h4>
                    <hr>
                    <table class="shop-table cart-table">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th class="d-flex justify-content-end">Image</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders->orderitems as $item)
                            <tr>
                                <th>{{ $item->products->name }}</th>
                                <th>{{ $item->quantity }}</th>
                                <th>{{ $item->price }}</th>
                                <th class="d-flex justify-content-end"><img src="{{ asset('storage/images/products/'.$item->products->image1) }}" width="50px" alt=""></th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-between">
                        <h5>Total:</h5>
                        <h5>{{ $orders->total_price }}</h5>
                    </div>
                    @if ($orders->status == '0')
                    <div class="d-flex justify-content-between">
                        <h5>Order:</h5>
                        <a href="{{ route('orders.remove',['id'=>$orders->id]) }}" class="btn btn-sm btn-warning" title="Remove this product">
                            <i class="fas fa-times"></i>Cancel
                        </a>
                    </div>
                    @endif
                </div>
            </div>
        </div>
    </div>
    

</x-frontend.layouts.master>