<x-frontend.layouts.master>

    <x-slot name="pagetitle">
        View Wishlist
    </x-slot>

    <div class="page-content pt-7 pb-10">
        <div class="container mt-7 mb-2">
            @if($wishlistItems->count() > 0)
                <div class="row">
                    <div class="col-lg-12 col-md-12 pr-lg-4">
                        <table class="shop-table cart-table">
                            <thead>
                                <tr>
                                    <th><span>Product</span></th>
                                    <th><span>Name</span></th>
                                    <th><span>Price</span></th>
                                </tr>
                            </thead>
                            <tbody>
                                @php
                                    $total = 0;
                                @endphp
                                @foreach ($wishlistItems as $item)                    
                                <tr class="product_data">
                                    <td class="product-thumbnail">
                                        <figure>
                                            <a href="#">
                                                <img src="{{ asset('storage/images/products/'.$item->products->image1) }}" width="100" height="100"
                                                    alt="product">
                                            </a>
                                        </figure>
                                    </td>
                                    <td class="product-name">
                                        <div class="product-name-section">
                                            <a href="#">{{ $item->products->name }}</a>
                                        </div>
                                    </td>
                                    <td class="product-price">
                                        <span class="amount">TK {{ $item->products->price }}</span>
                                    </td>
                                    <td>
                                        <input type="hidden" class="prod_id" value="{{ $item->prod_id }}">
                                        <input type="hidden" id="prod_color" value="{{ $item->prod_color }}">
                                        @if ($item->products->quantity > 0)
                                        <div class="input-group texr-center" style="width: 130px">
                                            <span class="input-group-text decrement-btn mt-2">-</span>
                                            <input type="text" name="quantity" class="form-control text-center qty-input" value="1">
                                            <span class="input-group-text increment-btn mt-2">+</span>
                                        </div>
                                        @else
                                        <div class="input-group texr-center" style="width: 130px">
                                            <h6>Out Of Stock</h6>
                                        </div>
                                         @endif
                                    </td>
                                    <td>
                                        @if ($item->products->quantity > 0 && $item->products->featured == 0)
                                            
                                        <button class="btn addToCartBtn btn-primary btn-sm add-to-cart"> <i class="fa fa-shopping-cart"></i> Add To Cart</button>
                                        @endif
                                    </td>
                                    <td>
                                        <button class="btn btn-danger btn-sm delete-wishlist-item"> <i class="fa fa-trash"></i> Remove</button>
                                    </td>
                                </tr>
                                @endforeach
    
                            </tbody>
                        </table>
                </div>
                @else
                    <div class="card-body text-center">
                         <h2>Your <i class="fa fa-shopping-cart"> Wishlist is empty</i></h2>
                    </div>
                    <div class="cart-actions mb-6 pt-4">
                        <a href="{{ route('category-filter') }}" class="btn btn-dark btn-md btn-rounded btn-icon-left mr-4 mb-4"><i
                                class="d-icon-arrow-left"></i>Continue Shopping</a>
                    </div>
            @endif
        </div>
    </div>

</x-frontend.layouts.master>