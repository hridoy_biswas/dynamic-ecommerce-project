<x-backend.layouts.master>
    <x-slot name="page_title">
        Sub Categories
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Sub Categories
            </x-slot>
            <x-slot name="add">
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Sub Categories</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table me-1"></i>Sub Categories</span>
            @canany(['admin','editor'])
                <span>
                    <a class="btn btn-sm btn-primary text-left" href="{{ Route('sub-categories.create') }}" role="button">Add</a>
                    <a class="btn btn-sm btn-danger text-left" href="{{ route('sub-categories.trash') }}" role="button">Trash</a>
                    {{-- <a class="btn btn-sm btn-info text-left" href="{{ route('sub-categories.excel') }}" role="button">Excel</a>
                    <a class="btn btn-sm btn-info text-left" href="{{ route('sub-categories.pdf') }}" role="button">Pdf</a> --}}
                </span>
            @endcanany
        </div>
    </div>
    <div class="card-body">
        {{-- <form action="{{ route('categories.index') }}" method="GET">
            <x-backend.form.search name="search" placeholder="Search" style="width: 220px;"/>
        </form> --}}
        <x-backend.layouts.elements.message :message="session('message')"/>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>SL#</th>
                    <th>Category Name</th>
                    <th>Name</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $sl = 0;
                @endphp
                @foreach ($subcategories as $subcategory)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $subcategory->categories->name }}</td>
                        <td>{{ $subcategory->name }}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{ route('sub-categories.show', ['sub_category'=>$subcategory->id]) }}" role="button">Show</a>
                            @canany(['admin','editor'])
                                <a class="btn btn-sm btn-warning" href="{{ route('sub-categories.edit', ['sub_category'=>$subcategory->id]) }}" role="button">Edit</a>
                                <form style="display: inline;" action="{{ route('sub-categories.destroy', ['sub_category'=>$subcategory->id]) }}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                                </form>
                            @endcanany
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
        {{ $subcategories->links() }}
    </div>
</div>

</x-backend.layouts.master>



