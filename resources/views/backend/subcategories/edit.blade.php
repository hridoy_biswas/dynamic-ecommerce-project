<x-backend.layouts.master>
    <x-slot name="page_title">
        Sub Categories
    </x-slot>
    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Sub Categories
            </x-slot>
            <x-slot name="add">  
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('categories.index') }}">Sub Categories</a></li>
            <li class="breadcrumb-item active">Edit</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header ">
            <div class="d-flex justify-content-between">
                <span><i class="fas fa-table me-1"></i>Sub Categories</span>
                <span>
                    <a class="btn btn-primary text-left" href="{{ Route('categories.index') }}" role="button">List</a>
                </span>
            </div>
        </div>
        <div class="card-body">
            <x-backend.layouts.elements.errors :errors="$errors"/>
            <form action="{{ route('sub-categories.update', ['sub_category' => $subcategory->id]) }}" method="POST">
                @csrf
                @method('patch')
                <div class="row mb-3">
                    <div class="col-md-6 mb-2">
                        <x-backend.form.select name="category" label="Sub Category" :option="$categories" select="{{ $subcategory->category_id }}"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="name" :value="old('name',$subcategory->name)"/>
                    </div>
                </div>

                <div class="mt-4 mb-0">
                    <div class="d-grid"><button onclick="return confirm('Are you sure want to update ?')" class="btn btn-primary btn-block" type="submit">Update</button></div>
                </div>
            </form>
        </div>
    </div>

</x-backend.layouts.master>