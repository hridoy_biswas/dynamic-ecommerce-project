<x-backend.layouts.master>
    <x-slot name="page_title">
        Products
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Products
            </x-slot>
            <x-slot name="add">
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Products</a></li>
            <li class="breadcrumb-item active">Show</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table me-1"></i>Products</span>
            <span>
                <a class="btn btn-primary text-left" href="{{ Route('products.index') }}" role="button">List</a>
            </span>
        </div>
    </div>
    <div class="card-body">
        <div class="container">
            <p>Product ID : {{ $product->id }}</p>
            <p>Name :{{ $product->name }}</p>
            <p>Short Description :{{ $product->short_description }}</p>
            <p>Long Description :{!! $product->long_description !!}</p>
            {{-- <p>Category Name:{{ $product->subcategory->category->name }}</p> --}}
            <p>Sub Category Name:{{ $product->subcategory->name }}</p>
            <p>Brand:{{ $product->brand->name ?? '' }}</p>
            <p>Price :{{ $product->price }}</p>
            <p>Quantity :{{ $product->quantity }}</p>
            <p>SKU :{{ $product->sku }}</p>
            <p>
                Tag : 
            @foreach ($product->tags as $tag)
            <tr>
                <td>{{ $loop->iteration }}:</td>
                <td>{{ $tag->title }}</td>
            </tr>
            @endforeach
            </p>
            <p>
                Size : 
            @foreach ($product->sizes as $size)
            <tr>
                <td>{{ $loop->iteration }}:</td>
                <td>{{ $size->title }}</td>
            </tr>
            @endforeach
            </p>
            <p>
                Color : 
            @foreach ($product->colors as $color)
            <tr>
                <td>{{ $loop->iteration }}:</td>
                <td>{{ $color->name }}</td>
            </tr>
            @endforeach
            </p>
            <p>Image1 :<img src="{{ asset('storage/images/products/'.$product->image1) }}" alt="" style="width: 100px; height:100px"></p>
            <p>Image2 :<img src="{{ asset('storage/images/products/'.$product->image2) }}" alt="" style="width: 100px; height:100px"></p>
            <p>Image3 :<img src="{{ asset('storage/images/products/'.$product->image3) }}" alt="" style="width: 100px; height:100px"></p>
            <p>image4 :<img src="{{ asset('storage/images/products/'.$product->image4) }}" alt="" style="width: 100px; height:100px"></p>            
        </div>
    </div>
</div>

</x-backend.layouts.master>



