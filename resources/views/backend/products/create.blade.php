<x-backend.layouts.master>
    <x-slot name="page_title">
        Products
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Products
            </x-slot>
            <x-slot name="add">
                
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('products.index') }}">Products</a></li>
            <li class="breadcrumb-item active">Create</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header ">
            
            <div class="d-flex justify-content-between">
                <span><i class="fas fa-table me-1"></i>Products</span>
                <span>
                    <a class="btn btn-primary text-left" href="{{ Route('products.index') }}" role="button">List</a>
                </span>
            </div>
        </div>
        <div class="card-body">
            <x-backend.layouts.elements.errors :errors="$errors"/>
            <form action="{{ route('products.store') }}" method="POST" enctype="multipart/form-data">
                @csrf
                <div class="row mb-3">
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="name" :value="old('name')"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="price" type="number" :value="old('price')"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="discount" type="number" :value="old('discount')"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="quantity" type="number" :value="old('quantity')"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.select name="category_id" label="Category" :option="$categories"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.select name="subcategory_id" label="Sub Category" :option="$subcategories"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.select name="brand_id" label="Brand" :option="$brands"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <p>Tag:</p>
                        @foreach ($tags as $key=>$value)
                        <x-backend.form.checkbox name="tag_id[]" value="{{ $key }}" label="{{ $value }}"
                        select="{{ in_array($key, old('tag_id')??[]) ? 'checked':'' }}"/>
                        @endforeach
                    </div>
                    <div class="col-md-6 mb-2">
                        <p>Size:</p>
                        @foreach ($sizes as $key=>$value)
                        <x-backend.form.checkbox name="size_id[]" value="{{ $key }}" label="{{ $value }}"
                        select="{{ in_array($key, old('size_id')??[]) ? 'checked':'' }}"/>
                        @endforeach                    
                    </div>
                    <div class="col-md-6 mb-2">
                        <p>Color:</p>
                        @foreach ($colors as $key=>$value)
                        <x-backend.form.checkbox name="color_id[]" value="{{ $key }}" label="{{ $value }}"
                        select="{{ in_array($key, old('color_id')??[]) ? 'checked':'' }}"/>
                        @endforeach                    
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="sku" :value="old('sku')"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.textarea name="short_description">
                            {{ old('short_description') }}
                        </x-backend.form.textarea>
                    </div>
                    <div class="col-md-12 mb-2">
                        <x-backend.form.textarea name="long_description">
                            {{ old('long_description') }}
                        </x-backend.form.textarea>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="image1" type="file"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="image2" type="file"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="image3" type="file"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="image4" type="file"/>
                    </div>
                    {{-- <div class="col-md-6 mb-2">
                        <label for="our_feature" class="form-label">Our Feature</label><br>
                        <select name="our_feature"  class="form-select" style="height: 38px; border:1px solid 	#DCDCDC; border-radius:5px; width:100%;">
                            <option value="">Choose... </option>
                            <option value="1" {{ old('our_feature') == '1' ? 'selected': '' }}>Our Feature Product</option>
                            @error('our_feature')
                            <span class="sm text-danger">{{ $message }}</span>
                        @enderror
                        </select>
                    </div> --}}

                    @push('script')
                    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

                    <script>
                        tinymce.init({
                        selector: '#long_description'
                        });
                    </script>
                    @endpush

                </div>
                <div class="mt-4 mb-0">
                    <div class="d-grid"><button class="btn btn-primary btn-block" type="submit">Save</button></div>
                </div>
            </form>
        </div>
    </div>

</x-backend.layouts.master>