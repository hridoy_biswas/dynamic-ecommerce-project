<x-backend.layouts.master>


    <x-slot name="page_title">
        Products
    </x-slot>
    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Products
            </x-slot>
            <x-slot name="add">  
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ Route('products.index') }}">Products</a></li>
            <li class="breadcrumb-item active">Edit</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header ">
            <div class="d-flex justify-content-between">
                <span><i class="fas fa-table me-1"></i>Products</span>
                <span>
                    <a class="btn btn-primary text-left" href="{{ Route('products.index') }}" role="button">List</a>
                </span>
            </div>
        </div>
        <div class="card-body">
            <x-backend.layouts.elements.errors :errors="$errors"/>
            <form action="{{ route('products.update', ['product' => $product->id]) }}" method="POST" enctype="multipart/form-data">
                @csrf
                @method('patch')
                <div class="row mb-3">
                    <div class="col-md-6">
                        <x-backend.form.input name="name" :value="old('name',$product->name)"/>
                    </div>
                    <div class="col-md-6">
                        <x-backend.form.input name="price" :value="old('price',$product->price)"/>
                    </div>
                    <div class="col-md-6">
                        <x-backend.form.input name="discount" :value="old('discount',$product->discount)"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="quantity" type="number" :value="old('quantity',$product->quantity)"/>
                    </div>
                    <div class="col-md-6">
                        <x-backend.form.select name="category_id" label="Category" :option="$categories" select="{{ $product->category_id }}"/>
                    </div>
                    <div class="col-md-6">
                        <x-backend.form.select name="subcategory_id" label="Sub Category" :option="$subcategories" select="{{ $product->subcategory_id }}"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.select name="brand_id" label="Brand" :option="$brands" select="{{ $product->brand_id }}"/>
                    </div>
                    <div class="col-md-6 mt-2">
                        <p>Tags:</p>
                        @foreach ($tags as $key=>$value)
                        <x-backend.form.checkbox name="tag_id[]" value="{{ $key }}" label="{{ $value }}" 
                            select="{{ in_array($key, old('tag_id')??$selectedTags) ? 'checked':'' }}"/>
                        @endforeach
                    </div>
                    <div class="col-md-6 mb-2">
                        <p>Sizes:</p>
                        @foreach ($sizes as $key=>$value)
                        <x-backend.form.checkbox name="size_id[]" 
                            value="{{ $key }}" label="{{ $value }}"
                            select="{{ in_array($key, old('size_id')??$selectedSizes) ? 'checked':'' }}"/>
                        @endforeach                     
                    </div>
                    <div class="col-md-6 mb-2">
                        <p>Colors:</p>
                        @foreach ($colors as $key=>$value)
                        <x-backend.form.checkbox name="color_id[]" value="{{ $key }}" label="{{ $value }}" 
                            select="{{ in_array($key, old('color_id')??$selectedColors) ? 'checked':'' }}"/>
                        @endforeach                    
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.input name="sku" :value="old('sku',$product->sku)"/>
                    </div>
                    <div class="col-md-6 mb-2">
                        <x-backend.form.textarea name="short_description">
                            {{ old('short_description',$product->short_description) }}
                        </x-backend.form.textarea>
                    </div>
                    <div class="col-md-12 mb-2">
                        <x-backend.form.textarea name="long_description">
                            {{ old('long_description',$product->long_description) }}
                        </x-backend.form.textarea>
                    </div>
                    <div class="col-md-6">
                        <x-backend.form.input name="image1" type="file"/>
                    </div>
                    <div class="col-md-6">
                        <x-backend.form.input name="image2" type="file"/>
                    </div>
                    <div class="col-md-6">
                        <x-backend.form.input name="image3" type="file"/>
                    </div>
                    <div class="col-md-6">
                        <x-backend.form.input name="image4" type="file"/>
                    </div>
                    {{-- <div class="col-md-6 mb-2">
                        <label for="our_feature" class="form-label">Our Feature</label><br>
                        <select name="our_feature"  class="form-select" style="height: 38px; border:1px solid 	#DCDCDC; border-radius:5px; width:100%;">
                            <option value="">Choose... </option>
                            <option value="1" {{ old('our_feature',$product->featured) == '1' ? 'selected': '' }}>Our Feature Product</option>
                            @error('our_feature')
                            <span class="sm text-danger">{{ $message }}</span>
                        @enderror
                        </select>
                    </div> --}}
                
                </div>
                <div class="mt-4 mb-0">
                    <div class="d-grid"><button onclick="return confirm('Are you sure want to update ?')" class="btn btn-primary btn-block" type="submit">Update</button></div>
                </div>
            </form>
        </div>
    </div>

    @push('script')
    <script src="https://cdn.tiny.cloud/1/no-api-key/tinymce/5/tinymce.min.js" referrerpolicy="origin"></script>

    <script>
        tinymce.init({
          selector: '#long_description'
        });
      </script>
      @endpush

</x-backend.layouts.master>