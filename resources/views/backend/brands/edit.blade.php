<x-backend.layouts.master>
    <x-slot name="page_title">
        Brands
    </x-slot>
    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Brands
            </x-slot>
            <x-slot name="add">  
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('brands.index') }}">Brands</a></li>
            <li class="breadcrumb-item active">Edit</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header ">
            <div class="d-flex justify-content-between">
                <span><i class="fas fa-table me-1"></i>Brands</span>
                <span>
                    <a class="btn btn-primary text-left" href="{{ Route('brands.index') }}" role="button">List</a>
                </span>
            </div>
        </div>
        <div class="card-body">
            <x-backend.layouts.elements.errors :errors="$errors"/>
            <form action="{{ route('brands.update', ['brand' => $brand->id]) }}" method="POST">
                @csrf
                @method('patch')
                <div class="row mb-3">
                    <div class="col-md-12">
                        <x-backend.form.input name="name" :value="old('name',$brand->name)"/>
                    </div>

                    <div class="col-md-12">
                        <x-backend.form.input name="description" :value="old('description',$brand->description)"/>
                    </div>
                </div>

                <div class="mt-4 mb-0">
                    <div class="d-grid"><button onclick="return confirm('Are you sure want to update ?')" class="btn btn-primary btn-block" type="submit">Update</button></div>
                </div>
            </form>
        </div>
    </div>

</x-backend.layouts.master>