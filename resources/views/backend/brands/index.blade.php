<x-backend.layouts.master>
    <x-slot name="page_title">
        Brands
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Brands
            </x-slot>
            <x-slot name="add">
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Brands</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table me-1"></i>Brands</span>
            @canany(['admin','editor'])
                <span>
                    <a class="btn btn-sm btn-primary text-left" href="{{ Route('brands.create') }}" role="button">Add</a>
                    <a class="btn btn-sm btn-danger text-left" href="{{ route('brands.trash') }}" role="button">Trash</a>
                    <a class="btn btn-sm btn-info text-left" href="{{ route('brands.excel') }}" role="button">Excel</a>
                    <a class="btn btn-sm btn-info text-left" href="{{ route('brands.pdf') }}" role="button">Pdf</a>
                </span>
            @endcanany
        </div>
    </div>
    <div class="card-body">
        <form action="{{ route('brands.index') }}" method="GET">
            <x-backend.form.search name="search" placeholder="Search" style="width: 220px;"/>
        </form>
        <x-backend.layouts.elements.message :message="session('message')"/>
        <table class="table table-striped">
            <thead>
                <tr>
                    <th>SL#</th>
                    <th>Name</th>
                    <th>Description</th>
                    <th>Action</th>
                </tr>
            </thead>
            <tbody>
                @php
                    $sl = 0;
                @endphp
                @foreach ($brands as $brand)
                    <tr>
                        <td>{{ ++$sl }}</td>
                        <td>{{ $brand->name }}</td>
                        <td>{{ $brand->description }}</td>
                        <td>
                            <a class="btn btn-sm btn-primary" href="{{ route('brands.show', ['brand'=>$brand->id]) }}" role="button">Show</a>
                            @canany(['admin','editor'])
                                <a class="btn btn-sm btn-warning" href="{{ route('brands.edit', ['brand'=>$brand->id]) }}" role="button">Edit</a>
                                <form style="display: inline;" action="{{ route('brands.destroy', ['brand'=>$brand->id]) }}" method="POST">
                                    @csrf
                                    @method('delete')
                                    <button onclick="return confirm('Are you sure want to delete ?')" class="btn btn-sm btn-danger" type="submit">Delete</button>
                                </form>
                            @endcanany
                        </td>
                    </tr>
                @endforeach
            </tbody>
        </table>
    </div>
</div>

</x-backend.layouts.master>



