<x-backend.layouts.master>

    <x-slot name="page_title">
        Orders
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Orders
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">users</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
    <div class="card mb-4">
        <div class="card-header ">
            <div class="d-flex justify-content-between">
                <span><i class="fas fa-table me-1"></i>Show</span>
                <a class="btn btn-primary text-left" href="{{ Route('orders.index') }}" role="button">List</a>
            </div>
        </div>
        <div class="card-body">
            <div class="row">
                <div class="col-md-6">
                    <h4>Shipping Details</h4>
                    <hr>
                    <div class="border p-2">
                        <label for="">Name</label>
                        <div class="border p-2">{{ $orders->name }}</div>
                        <label for="">Email</label>
                        <div class="border p-2">{{ $orders->email }}</div>
                        <label for="">Phone</label>
                        <div class="border p-2">{{ $orders->phone }}</div>
                        <label for="">Address</label>
                        <div class="border p-2">{{ $orders->address }}</div>
                        <label for="">City</label>
                        <div class="border p-2">{{ $orders->city }}</div>
                        <label for="">Area</label>
                        <div class="border p-2">{{ $orders->area }}</div>
                        <label for="">Country</label>
                        <div class="border p-2">{{ $orders->country }}</div>
                        <label for="">Post Code</label>
                        <div class="border p-2">{{ $orders->postcode }}</div>
                    </div>
                </div>
                <div class="col-md-6">
                    <h4>Order Details</h4>
                    <hr>
                    <table class="table table-bordered">
                        <thead>
                            <tr>
                                <th>Name</th>
                                <th>Quantity</th>
                                <th>Price</th>
                                <th class="d-flex justify-content-end">Image</th>
                            </tr>
                        </thead>
                        <tbody>
                            @foreach ($orders->orderitems as $item)
                            <tr>
                                <th>{{ $item->products->name }}</th>
                                <th>{{ $item->quantity }}</th>
                                <th>{{ $item->price }}</th>
                                <th class="d-flex justify-content-end"><img src="{{ asset('storage/images/products/'.$item->products->image1) }}" width="50px" alt=""></th>
                            </tr>
                            @endforeach
                        </tbody>
                    </table>
                    <div class="d-flex justify-content-between mb-2">
                        <h5>Total:</h5>
                        <h5>{{ $orders->total_price }}</h5>
                    </div>
                    <form action="{{ route('orders.update',['id'=>$orders->id]) }}" method="POST">
                        @csrf
                        <label for="">Status</label>
                        <select class="form-select" name="status">
                            <option {{ $orders->status == '0' ? 'selected' : '' }} value="0">Panding</option>
                            <option {{ $orders->status == '1' ? 'selected' : '' }} value="1">Shipping</option>
                            <option {{ $orders->status == '2' ? 'selected' : '' }} value="2">Complate</option>
                            <option {{ $orders->status == '3' ? 'selected' : '' }} value="3">Cancel</option>
                        </select>
                        <div class="mt-4 mb-0">
                            <div class="d-grid">
                                <button onclick="return confirm('Are you sure want to update ?')" class="btn btn-primary btn-block" type="submit">Update</button>
                            </div>
                        </div>
                    </form>
                </div>
            </div>
        </div>
    </div>

</x-backend.layouts.master>



