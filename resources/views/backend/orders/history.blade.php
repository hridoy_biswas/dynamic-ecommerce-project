<x-backend.layouts.master>

    <x-slot name="page_title">
        Complete Order
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Complete Order
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item active">Complete Order</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
    <div class="card mb-4">
        <div class="card-header ">
            <div class="d-flex justify-content-between">
                <span><i class="fas fa-table me-1"></i>Complete Order</span>
                <span><a class="btn btn-sm btn-success text-left" href="{{ Route('orders.index') }}" role="button">New Order</a></span>
            </div>
        </div>
        <div class="card-body">
            <x-backend.layouts.elements.message :message="session('message')"/>
            <table class="table table-striped">
                <thead>
                    <tr>
                        <tr>
                            <th>Order Date</th>
                            <th>Tracking Number</th>
                            <th>Total Price</th>
                            <th>Status</th>
                            <th class="d-flex justify-content-end">Action</th>
                        </tr>
                    </tr>
                </thead>
                <tbody>
                    @foreach ($orders as $item)
                        <tr>
                            <td>{{ date('d-m-y', strtotime($item->created_at)) }}</td>
                            <td>{{ $item->tracking_no }}</td>
                            <td>{{ $item->total_price }}</td>
                            <td>{{ $item->status == '0' ? 'Pending' : ($item->status == '1' ? 'Shipping' :($item->status == '2' ? 'Complate': 'Cancel')) }}</td>
                            <td class="d-flex justify-content-end"><a href="{{ route('orders.show', ['id' => $item->id]) }}"  class="btn btn-dark btn-md btn-rounded btn-icon-left mr-4 mb-4">View</a></td>
                        </tr>
                    @endforeach

                </tbody>
            </table>
        </div>
    </div>

</x-backend.layouts.master>



