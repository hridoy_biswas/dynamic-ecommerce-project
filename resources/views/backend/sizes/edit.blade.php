<x-backend.layouts.master>
    <x-slot name="page_title">
        Sizes
    </x-slot>
    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Sizes
            </x-slot>
            <x-slot name="add">  
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('sizes.index') }}">Sizes</a></li>
            <li class="breadcrumb-item active">Edit</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header ">
            <div class="d-flex justify-content-between">
                <span><i class="fas fa-table me-1"></i>Sizes</span>
                <span>
                    <a class="btn btn-primary text-left" href="{{ Route('sizes.index') }}" role="button">List</a>
                </span>
            </div>
        </div>
        <div class="card-body">
            <x-backend.layouts.elements.errors :errors="$errors"/>
            <form action="{{ route('sizes.update', ['size' => $size->id]) }}" method="POST">
                @csrf
                @method('patch')
                <div class="row mb-3">
                    <div class="col-md-12">
                        <x-backend.form.input name="title" :value="old('title',$size->title)"/>
                    </div>

                    <div class="col-md-12">
                        <x-backend.form.input name="description" :value="old('description',$size->description)"/>
                    </div>
                </div>

                <div class="mt-4 mb-0">
                    <div class="d-grid"><button onclick="return confirm('Are you sure want to update ?')" class="btn btn-primary btn-block" type="submit">Update</button></div>
                </div>
            </form>
        </div>
    </div>

</x-backend.layouts.master>