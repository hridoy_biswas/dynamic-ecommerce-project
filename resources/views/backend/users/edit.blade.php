<x-backend.layouts.master>
    <x-slot name="page_title">
        Users
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Users
            </x-slot>
            <x-slot name="add">
                
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
                <li class="breadcrumb-item active">Users</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>

    <div class="card mb-4">
        <div class="card-header ">
            
            <div class="d-flex justify-content-between">
                <span><i class="fas fa-table me-1"></i>Users</span>
                <span>
                    <a class="btn btn-primary text-left" href="{{ Route('users.index') }}" role="button">List</a>
                </span>
            </div>
            @if(Session::has('message'))
            <p class="alert alert-info">{{ Session::get('message') }}</p>
            @endif
        </div>
        <div class="card-body">
            
            <form action="{{ route('users.update',['user' => $user->id]) }}" method="POST">
                @csrf
                @method('patch')
                <x-backend.form.input name="name" :value="$user->name"/>

                <div class="form-group">
                    <label for="name">Role</label><br>
                    <select name="role_id" class="form-select">
                        <option value=""  style="border: 1px solid #DCDCDC;">Select One</option>
                        @foreach ($roles as $role)
                            <option value="{{ $role->id }}" {{ $user->role_id==$role->id?'selected':'' }}>{{ $role->name }}</option>
                        @endforeach
                      </select>
                  </div>

                <div class="mt-4 mb-0">
                    <div class="d-grid"><button class="btn btn-primary btn-block" type="submit">Save</button></div>
                </div>
            </form>
        </div>
    </div>

</x-backend.layouts.master>