<x-backend.layouts.master>
    <x-slot name="page_title">
        Colors
    </x-slot>

    <x-slot name="breadcrumb">
        <x-backend.layouts.elements.breadcrumb>
            <x-slot name="pageHeader">
                Colors
            </x-slot>
            <x-slot name="add">
            </x-slot>
            <li class="breadcrumb-item"><a href="{{ route('dashboard') }}">Dashboard</a></li>
            <li class="breadcrumb-item"><a href="{{ route('colors.index') }}">Colors</a></li>
            <li class="breadcrumb-item active">Show</li>
        </x-backend.layouts.elements.breadcrumb>
    </x-slot>
    
<div class="card mb-4">
    <div class="card-header ">
        <div class="d-flex justify-content-between">
            <span><i class="fas fa-table me-1"></i>Colors</span>
            <span>
                <a class="btn btn-primary text-left" href="{{ Route('colors.index') }}" role="button">List</a>
            </span>
        </div>
    </div>
    <div class="card-body">
        <p>ID :{{ $color->id }}</p>
        <p>Name :{{ $color->name }}</p>
    </div>
</div>

</x-backend.layouts.master>



