<?php

use App\Http\Controllers\BrandController;
use App\Http\Controllers\CartController;
use App\Http\Controllers\CategoryController;
use App\Http\Controllers\CheckoutController;
use App\Http\Controllers\ColorController;
use App\Http\Controllers\CommentController;
use App\Http\Controllers\FrontendController;
use App\Http\Controllers\NotificationController;
use App\Http\Controllers\OrderController;
use App\Http\Controllers\ProductController;
use App\Http\Controllers\ProfileController;
use App\Http\Controllers\RoleController;
use App\Http\Controllers\SizeController;
use App\Http\Controllers\SubCategoryController;
use App\Http\Controllers\TagController;
use App\Http\Controllers\UserController;
use App\Http\Controllers\WishlistController;
use Illuminate\Support\Facades\Route;

Route::get('/', [FrontendController::class, 'index'])->name('home');
Route::get('/product-details/{product}', [FrontendController::class, 'singleProduct'])->name('product-details');
Route::get('/shop/{sub_category}', [FrontendController::class, 'show'])->name('sub-categories.shop');
Route::get('/categories-show.shop/{category}', [FrontendController::class, 'allshow'])->name('categories-show.shop');
Route::get('/sub-categories-show.shop/{sub_category}', [FrontendController::class, 'subshow'])->name('sub-categories-show.shop');
Route::get('/category-filter', [FrontendController::class, 'filter'])->name('category-filter');

//cart
Route::post('add-to-cart', [CartController::class, 'addToCart']);
Route::post('delete-cart-item', [CartController::class, 'deleteProduct']);
Route::post('update-cart', [CartController::class, 'updateCart']);

//wishlist
Route::post('add-to-wishlist', [WishlistController::class, 'addToWishlist']);
Route::post('delete-wishlist-item', [WishlistController::class, 'deleteWishlist']);

Route::middleware(['auth','web'])->group(function () {
    //frontend route start
    Route::get('/view-cart', [CartController::class, 'viewCart'])->name('view.cart');
    Route::get('/view-wish', [WishlistController::class, 'viewWishlist'])->name('view.wishlist');
    Route::get('/checkout', [CheckoutController::class, 'index'])->name('checkout');
    Route::post('/place-order', [CheckoutController::class, 'placeOrder'])->name('placeOrder');
    Route::get('/my-order', [FrontendController::class, 'myOrder'])->name('my-order');
    Route::get('/view-order/{id}', [FrontendController::class, 'viewOrder'])->name('view-order');
    Route::get('/user-profile.edit', [FrontendController::class, 'edit'])->name('profile.edit');
    Route::get('/user/profile.edit', [ProfileController::class, 'edit'])->name('user.profile.edit');
    //comment
    Route::post('commnets/{id}', [CommentController::class, 'store'])->name('comment.store');
    //frontend route End
    
    
    //crud route start

    Route::get('/dashboard', function () {
        return view('backend.dashboard');
    })->name('dashboard');

    //orders crud
    Route::get('/orders', [OrderController::class, 'index'])->name('orders.index');
    Route::get('/orders/{id}', [OrderController::class, 'show'])->name('orders.show');
    Route::post('/orders/update/{id}', [OrderController::class, 'update'])->name('orders.update');
    Route::get('/orders-history', [OrderController::class, 'history'])->name('orders.history');
    Route::get('/orders-cancel', [OrderController::class, 'cancel'])->name('orders.cancel');
    Route::get('/orders.remove/{id}', [OrderController::class, 'remove'])->name('orders.remove');



    //roles crud
    Route::get('/roles', [RoleController::class, 'index'])->name('roles.index');
    Route::get('/roles/{role}', [RoleController::class, 'show'])->name('roles.show');

    //notification
    Route::get('/notifications/{notification}', [NotificationController::class, 'show'])->name('notifications.show');

    //users crud
    Route::resource('users', UserController::class)->except(['create', 'store']);
    Route::get('/users-trash', [UserController::class, 'trash'])->name('users.trash');
    Route::get('/users-trash/{user}', [UserController::class, 'restore'])->name('users.restore');
    Route::get('/users-trash/{user}/delete', [UserController::class, 'delete'])->name('users.delete');
    Route::get('/users-excel', [UserController::class, 'excel'])->name('users.excel');
    Route::get('/users-pdf', [UserController::class, 'pdf'])->name('users.pdf');
    Route::get('/user/profile.edit', [ProfileController::class, 'edit'])->name('user.profile.edit');
    Route::patch('/user/profile.update', [ProfileController::class, 'update'])->name('user.profile.update');

    //category crud
    Route::get('/categories-trash', [CategoryController::class, 'trash'])->name('categories.trash');
    Route::get('/categories-trash/{category}', [CategoryController::class, 'restore'])->name('categories.restore');
    Route::get('/categories-trash/{category}/delete', [CategoryController::class, 'delete'])->name('categories.delete');
    Route::get('/categories-excel', [CategoryController::class, 'excel'])->name('categories.excel');
    Route::get('/categories-pdf', [CategoryController::class, 'pdf'])->name('categories.pdf');
    Route::resource('categories', CategoryController::class);

    //sub category crud
    Route::get('/sub-categories-trash', [SubCategoryController::class, 'trash'])->name('sub-categories.trash');
    Route::get('/sub-categories-trash/{sub_category}', [SubCategoryController::class, 'restore'])->name('sub-categories.restore');
    Route::get('/sub-categories-trash/{sub_category}/delete', [SubCategoryController::class, 'delete'])->name('sub-categories.delete');
    Route::resource('sub-categories', SubCategoryController::class);

    //product crud
    Route::get('/products-trash', [ProductController::class, 'trash'])->name('products.trash');
    Route::get('/products-trash/{product}', [ProductController::class, 'restore'])->name('products.restore');
    Route::get('/products-trash/{product}/delete', [ProductController::class, 'delete'])->name('products.delete');
    Route::get('/products-excel', [ProductController::class, 'excel'])->name('products.excel');
    Route::get('/products-pdf', [ProductController::class, 'pdf'])->name('products.pdf');
    Route::resource('products', ProductController::class);

    //tags crud
    Route::resource('tags', TagController::class);
    Route::get('/tags-trash', [TagController::class, 'trash'])->name('tags.trash');
    Route::get('/tags-trash/{tag}', [TagController::class, 'restore'])->name('tags.restore');
    Route::get('/tags-trash/{tag}/delete', [TagController::class, 'delete'])->name('tags.delete');
    Route::get('/tags-excel', [TagController::class, 'excel'])->name('tags.excel');
    Route::get('/tags-pdf', [TagController::class, 'pdf'])->name('tags.pdf');

    //sizes crud
    Route::resource('sizes', SizeController::class);
    Route::get('/sizes-trash', [SizeController::class, 'trash'])->name('sizes.trash');
    Route::get('/sizes-trash/{size}', [SizeController::class, 'restore'])->name('sizes.restore');
    Route::get('/sizes-trash/{size}/delete', [SizeController::class, 'delete'])->name('sizes.delete');
    Route::get('/sizes-excel', [SizeController::class, 'excel'])->name('sizes.excel');
    Route::get('/sizes-pdf', [SizeController::class, 'pdf'])->name('sizes.pdf');

    //colors crud
    Route::resource('colors', ColorController::class);
    Route::get('/colors-trash', [ColorController::class, 'trash'])->name('colors.trash');
    Route::get('/colors-trash/{color}', [ColorController::class, 'restore'])->name('colors.restore');
    Route::get('/colors-trash/{color}/delete', [ColorController::class, 'delete'])->name('colors.delete');
    Route::get('/colors-excel', [ColorController::class, 'excel'])->name('colors.excel');
    Route::get('/colors-pdf', [ColorController::class, 'pdf'])->name('colors.pdf');

    //brands crud
    Route::resource('brands', BrandController::class);
    Route::get('/brands-trash', [BrandController::class, 'trash'])->name('brands.trash');
    Route::get('/brands-trash/{brand}', [BrandController::class, 'restore'])->name('brands.restore');
    Route::get('/brands-trash/{brand}/delete', [BrandController::class, 'delete'])->name('brands.delete');
    Route::get('/brands-excel', [BrandController::class, 'excel'])->name('brands.excel');
    Route::get('/brands-pdf', [BrandController::class, 'pdf'])->name('brands.pdf');

    //crud route end
});

require __DIR__ . '/auth.php';
