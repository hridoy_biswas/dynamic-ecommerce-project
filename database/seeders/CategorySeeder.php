<?php

namespace Database\Seeders;

use App\Models\Category;
use Illuminate\Database\Seeder;

class CategorySeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Category::create([
            'name' => 'SmartPhones',
            'description' => 'SmartPhones'
        ]);
        Category::create([
            'name' => 'Electronices & Appliances',
            'description' => 'Electronices & Appliances'
        ]);
        Category::create([
            'name' => 'Mobile Accessories',
            'description' => 'Mobile Accessories'
        ]);
        Category::create([
            'name' => "Computer & Accessories",
            'description' => "Computer & Accessories"
        ]);
        Category::create([
            'name' => "LifeStyle",
            'description' => "LifeStyle"
        ]);
    }
}
