<?php

namespace Database\Seeders;

use App\Models\Brand;
use Illuminate\Database\Seeder;

class BrandSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Brand::create([
            'name' => 'Polo',
            'description' => 'Polo'
        ]);
        Brand::create([
            'name' => 'Adidas',
            'description' => 'Adidas'
        ]);
        Brand::create([
            'name' => 'Rise',
            'description' => 'Rise'
        ]);
        Brand::create([
            'name' => 'Yellow',
            'description' => 'Yellow'
        ]);
        Brand::create([
            'name' => 'Easy',
            'description' => 'Easy'
        ]);
        Brand::create([
            'name' => 'Freeland',
            'description' => 'Freeland'
        ]);
    }
}
