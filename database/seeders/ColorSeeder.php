<?php

namespace Database\Seeders;

use App\Models\Color;
use Illuminate\Database\Seeder;

class ColorSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        Color::create([
            'name' => 'Red'
        ]);
        Color::create([
            'name' => 'Green'
        ]);
        Color::create([
            'name' => 'Gray'
        ]);
        Color::create([
            'name' => 'Light Gray'
        ]);
        Color::create([
            'name' => 'Blue'
        ]);
    }
}
