$(document).ready(function () {

    $('.addToCartBtn').click(function (e) { 
        e.preventDefault();

        var product_id = $(this).closest('.product_data').find('.prod_id').val();
        var product_qty = $(this).closest('.product_data').find('.qty-input').val();
        var product_color = $(this).closest('.product_data').find('#prod_color').val();
        var product_size = $(this).closest('.product_data').find('#prod_size').val();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $.ajax({
            type: "POST",
            url: '/add-to-cart',
            data: {
                'product_id': product_id,
                'product_qty': product_qty ?? null,
                'product_color': product_color ?? null,
                'product_size': product_size ?? null,
            },
            success: function (response) {
                window.location.reload();
                swal(response.status);
            }
        });   
    });

    $('.delete-cart-item').click(function (e) { 
        e.preventDefault();
        var prod_id = $(this).closest('.product_data').find('.prod_id').val();
        var product_color = $(this).closest('.product_data').find('#prod_color').val();
        var product_size = $(this).closest('.product_data').find('#prod_size').val();

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: "delete-cart-item",
            data: {
                'prod_id' :prod_id,
                'product_color': product_color ?? null,
                'product_size': product_size ?? null,
            },
            success: function (response) {
                window.location.reload();
                swal(response.status);
            }
        });
    });

    $('.addToWishlistBtn').click(function (e) { 
        e.preventDefault();

        var product_id = $(this).closest('.product_data').find('.prod_id').val();
        var product_color = $(this).closest('.product_data').find('#prod_color').val();
        var product_size = $(this).closest('.product_data').find('#prod_size').val();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });
        
        $.ajax({
            type: "POST",
            url: '/add-to-wishlist',
            data: {
                'product_id': product_id,
                'product_color': product_color ?? null,
                'product_size': product_size ?? null,
            },
            success: function (response) {
                window.location.reload();
                swal(response.status);
            }
        });     
    });

    $('.delete-wishlist-item').click(function (e) { 
        e.preventDefault();
        var prod_id = $(this).closest('.product_data').find('.prod_id').val();
        var product_color = $(this).closest('.product_data').find('#prod_color').val();
        var product_size = $(this).closest('.product_data').find('#prod_size').val();
        
        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: "delete-wishlist-item",
            data: {
                'prod_id' :prod_id,
                'product_color': product_color ?? null,
                'product_size': product_size ?? null,
            },
            success: function (response) {
                window.location.reload();
                swal(response.status);
            }
        });
    });

    $('.increment-btn').click(function (e) { 
        e.preventDefault();

        var inc_value = $(this).closest('.product_data').find('.qty-input').val();
        var value = parseInt(inc_value, 10);
        value = isNaN(value) ? 0 : value;
        if(value < 10)
        {
            value++;
            $(this).closest('.product_data').find('.qty-input').val(value);
        }
    });


    $('.decrement-btn').click(function (e) { 
        e.preventDefault();

        var dc_value = $(this).closest('.product_data').find('.qty-input').val();
        var value = parseInt(dc_value, 10);
        value = isNaN(value) ? 0 : value;
        if(value > 1)
        {
            value--;
            $(this).closest('.product_data').find('.qty-input').val(value);
        }
    });



    $('.changeQuantity').click(function (e) { 
        e.preventDefault();
        var prod_id = $(this).closest('.product_data').find('.prod_id').val();
        var qty = $(this).closest('.product_data').find('.qty-input').val();
        data = {
            'prod_id':prod_id,
            'prod_qty':qty
        }

        $.ajaxSetup({
            headers: {
                'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
            }
        });

        $.ajax({
            type: "POST",
            url: "update-cart",
            data: data,
            success: function (response) {
                window.location.reload();
            }
        });
    });
});