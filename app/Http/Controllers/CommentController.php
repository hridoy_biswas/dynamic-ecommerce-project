<?php

namespace App\Http\Controllers;

use App\Models\Comment;
use App\Models\Product;
use App\Models\User;
use App\Notifications\NewComment;
use Illuminate\Http\Request;
use Illuminate\View\View;

class CommentController extends Controller
{
    public function store(Request $request, $id)
    {
        $product = Product::findOrFail($id);
        $comment = $product->comments()->create([
            'body' => $request->comment,
            'created_by' => auth()->user()->id
        ]);

        $users = User::where('role_id', 1)->get();
        foreach ($users as $user){
            $user->notify(new NewComment($comment));
        }
        return redirect()->back();
    }

    public function compose(View $view)
    {
        $user = User::find(1);
        $view->with('user', $user);
    }

   
}
