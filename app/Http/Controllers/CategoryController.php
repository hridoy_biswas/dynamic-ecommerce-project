<?php

namespace App\Http\Controllers;

use App\Exports\categoriesExport;
use App\Http\Requests\CategoryRequest;
use App\Models\category;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Excel;
use PDF;

class CategoryController extends Controller
{
    public function index()
    {
        try{
            $categoryCollection = Category::latest();
            if(request('search')){
                $category = $categoryCollection
                                ->where('title','like', '%'.request('search').'%')
                                ->orWhere('description','like', '%'.request('search').'%');
            }
                $category = $categoryCollection->paginate(5);
                return view('backend.categories.index',[
                'categories' => $category
            ]);
            
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function create()
    {
        return view('backend.categories.create');
    }

    public function store(CategoryRequest $request)
    {
        try{
            category::create([
                'name' => $request->name,
                'description' => $request->description
            ]);
            return redirect()->route('categories.index')->withMessage('Task was successful!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Category $category)
    {
        return view('backend.categories.show', compact('category'));
    }

    public function edit(Category $category)
    {
        return view('backend.categories.edit',[
            'category' =>$category
        ]);
    }

    public function update(CategoryRequest $request ,category $category)
    {
        try{
            $category->update([
                'name' => $request->name,
                'description' => $request->description
            ]);
            return redirect()->route('categories.index')->withMessage('Task was successfully Update!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Category $category)
    {
        try{
            $category->delete();
            return redirect()->route('categories.index')->withMessage('Task was successfully Delete!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function trash()
    {
        try{
            $category = Category::onlyTrashed()->get();
            return view('backend.categories.trash',[
                'categories' => $category
            ]);
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function restore($id)
    {
        try{
            $category = Category::onlyTrashed()->findOrFail($id);
            $category->restore();
            return redirect()->route('categories.trash')->withMessage('Task was successfully Restore!');
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function delete($id)
    {
        try{
            $category = Category::onlyTrashed()->findOrFail($id);
            $category->forceDelete();
            return redirect()->route('categories.trash')->withMessage('Task was successfully Permanent Delete!');
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function excel() 
    {
        return Excel::download(new categoriesExport, 'category.xlsx');
    }

    public function pdf()
    {
        $category = Category::all();
        $pdf = PDF::loadView('backend.categories.pdf', ['categories' => $category]);
        return $pdf->download('categories.pdf');
    }
}
