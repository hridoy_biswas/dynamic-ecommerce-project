<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CartController extends Controller
{
    public function addToCart(Request $request)
    {
        $product_id = $request->input('product_id');
        $product_qty = $request->input('product_qty');
        $product_color = $request->product_color;
        $product_size = $request->product_size;

        if(Auth::check()){
            $prod_check = Product::where('id',$product_id)->first();

            if($prod_check)
            {
                if(Cart::where('prod_id', $product_id)->where('user_id',Auth::id())->exists())
                {
                    return response()->json(['status' => $prod_check->name." Already Added to Cart"]);
                }else{
                   $cartItem = new Cart();
                   $cartItem->user_id = Auth::id();
                   $cartItem->prod_id = $product_id;
                   $cartItem->prod_qty = $product_qty;
                   $cartItem->prod_color = $product_color ?? 0;
                   $cartItem->prod_size = $product_size ?? 0;
                   $cartItem->save();
                    return response()->json(['status' => $prod_check->name."Add To Cart"]);
                }
            }
        }else
        {
            return response()->json(['status' =>"Login to Continue"]);
        }

    }

    public function viewCart()
    {
        $cartItems = Cart::where('user_id',Auth::id())->get();
        return view('frontend.view-cart', compact('cartItems'));
    }

    public function deleteProduct(Request $request)
    {
        if(Auth::check())
        {
            $prod_id = $request->input('prod_id');
            if(Cart::where('prod_id',$prod_id)->where('user_id',Auth::id())->exists())
            {
                $cartItem = Cart::where('prod_id',$prod_id)->where('user_id',Auth::id())->first();
                $cartItem->delete();
                return response()->json(['status' =>"Product Deleted Sucessfully"]);
            }
        }else
        {
            return response()->json(['status' =>"Login to Continue"]);
        }
    }

    public function updateCart(Request $request)
    {
        $prod_id = $request->input('prod_id');
        $prod_qty = $request->input('prod_qty');

        if(Auth::check())
        {
            if(Cart::where('prod_id',$prod_id)->where('user_id',Auth::id())->exists())
            {
                $cart = Cart::where('prod_id',$prod_id)->where('user_id',Auth::id())->first();
                $cart->prod_qty = $prod_qty;
                $cart->update();
                return response()->json(['status' =>"Quantity Update"]);
            }
        }
    }
}
