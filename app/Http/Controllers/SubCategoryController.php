<?php

namespace App\Http\Controllers;

use App\Models\Category;
use App\Models\Product;
use App\Models\SubCategory;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;

class SubCategoryController extends Controller
{
    public function index()
    {
        try{
            $categoryCollection = SubCategory::latest();
            if(request('search')){
                $subcategory = $categoryCollection
                                ->where('name','like', '%'.request('search').'%');
            }
                $subcategory = $categoryCollection->paginate(5);
                return view('backend.subcategories.index',[
                'subcategories' => $subcategory,
            ]);
            
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function create()
    {
        $categories = Category::orderBY('name','asc')
                            ->pluck('name','id')
                            ->toArray();
        return view('backend.subcategories.create', [
            'categories' => $categories
        ]);
    }

    public function store(Request $request)
    {
        $request->validate([
            'name' => 'required|unique:sub_categories,name'
        ]);

        try{
            SubCategory::create([
                'category_id' => $request->category,
                'name' => $request->name,
            ]);
            return redirect()->route('sub-categories.index')->withMessage('Task was successful!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show($subcategoryId)
    {
        $products = Product::where('subcategory_id', $subcategoryId)->get();
        return view('backend.subcategories.show', compact('products'));
    }

    public function edit($subcategoryId)
    {
        $categories = Category::orderBY('name','asc')
                            ->pluck('name','id')
                            ->toArray();
        $subcategory = SubCategory::findOrFail($subcategoryId);
        return view('backend.subcategories.edit',[
            'categories' =>$categories,
            'subcategory' => $subcategory
        ]);
    }

    public function update(Request $request ,$subcategoryId)
    {
        $request->validate([
            'name' => 'required',
        ]);

        $subcategory = SubCategory::findOrFail($subcategoryId);
        try{
            $subcategory->update([
                'category_id' => $request->category,
                'name' => $request->name,
            ]);
            return redirect()->route('sub-categories.index')->withMessage('Task was successfully Update!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy($subcategoryId)
    {
        $subcategory = SubCategory::findOrFail($subcategoryId);
        try{
            $subcategory->delete();
            return redirect()->route('sub-categories.index')->withMessage('Task was successfully Delete!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function trash()
    {
        try{
            $subcategories = SubCategory::onlyTrashed()->get();
            return view('backend.subcategories.trash',[
                'subcategories' => $subcategories
            ]);
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function restore($id)
    {
        try{
            $category = SubCategory::onlyTrashed()->findOrFail($id);
            $category->restore();
            return redirect()->route('sub-categories.trash')->withMessage('Task was successfully Restore!');
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function delete($id)
    {
        try{
            $category = SubCategory::onlyTrashed()->findOrFail($id);
            $category->forceDelete();
            return redirect()->route('sub-categories.trash')->withMessage('Task was successfully Permanent Delete!');
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }
}
