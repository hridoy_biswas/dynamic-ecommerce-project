<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\DB;

class NotificationController extends Controller
{
    public function show()
    {
        $notifications = DB::table('notifications')->paginate(10);

        foreach($notifications as $notification){
            $notifications = json_decode($notification->data, true);
        }
        return view('backend.notifications.show', compact('notifications'));
    }
}
