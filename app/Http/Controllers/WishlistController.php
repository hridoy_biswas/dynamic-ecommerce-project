<?php

namespace App\Http\Controllers;

use App\Models\Wishlist;
use App\Models\Product;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class WishlistController extends Controller
{
    public function addToWishlist(Request $request)
    {
        $product_id = $request->input('product_id');
        $product_color = $request->product_color;
        $product_size = $request->product_size;

        if(Auth::check()){
            $prod_check = Product::where('id',$product_id)->first();

            if($prod_check)
            {
                if(Wishlist::where('prod_id', $product_id)->where('user_id',Auth::id())->exists())
                {
                    return response()->json(['status' => $prod_check->name." Already Added to Wishlist"]);
                }else{
                   $wishlistItem = new Wishlist();
                   $wishlistItem->user_id = Auth::id();
                   $wishlistItem->prod_id = $product_id;
                   $wishlistItem->prod_color = $product_color ?? 0;
                   $wishlistItem->prod_size = $product_size ?? 0;
                   $wishlistItem->save();
                    return response()->json(['status' => $prod_check->name."Add To Wishlist"]);
                }
            }
        }else
        {
            return response()->json(['status' =>"Login to Continue"]);
        }

    }

    public function viewWishlist()
    {
        $wishlistItems = Wishlist::where('user_id',Auth::id())->get();
        return view('frontend.view-wishlist', compact('wishlistItems'));
    }

    public function deleteWishlist(Request $request)
    {
        if(Auth::check())
        {
            $prod_id = $request->input('prod_id');
            if(Wishlist::where('prod_id',$prod_id)->where('user_id',Auth::id())->exists())
            {
                $wishlistItem = Wishlist::where('prod_id',$prod_id)->where('user_id',Auth::id())->first();
                $wishlistItem->delete();
                return response()->json(['status' =>"Product Deleted Sucessfully"]);
            }
        }else
        {
            return response()->json(['status' =>"Login to Continue"]);
        }
    }
}
