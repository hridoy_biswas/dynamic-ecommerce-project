<?php

namespace App\Http\Controllers;

use App\Models\Cart;
use App\Models\Order;
use App\Models\OrderItem;
use App\Models\Product;
use Illuminate\Database\QueryException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class CheckoutController extends Controller
{
    public function index()
    {
        $old_cartItem = Cart::where('user_id',Auth::id())->get();
        foreach($old_cartItem as $item)
        {
            if(!Product::where('id', $item->prod_id)->where('quantity', '>=' ,$item->prod_qty)->exists())
            {
                $removeItem = Cart::where('user_id',Auth::id())->where('prod_id', $item->prod_id)->first();
                $removeItem->delete();
            }
        }

        $cartItem = Cart::where('user_id',Auth::id())->get();
        return view('frontend.checkout', compact('cartItem'));
    }

    public function placeOrder(Request $request)
    {
        try{
            $order = new Order();
            $order->delivery = $request->delivery;
            $order->user_id = Auth::id();
            $order->name = $request->name;
            $order->email = $request->email;
            $order->phone = $request->phone;
            $order->address = $request->address;
            $order->city = $request->city;
            $order->area = $request->area;
            $order->country = $request->country;
            $order->postcode = $request->postcode;

            $total = 0;
            $cartItems_total = Cart::where('user_id', Auth::id())->get();
            foreach($cartItems_total as $prod)
            {
                $total  += $prod->products->price * $prod->prod_qty;
            }
            $order->total_price = $total;

            $order->tracking_no = 'hello'.rand(1111,9999);
            $order->save();

            $cartItem = Cart::where('user_id',Auth::id())->get();
            foreach($cartItem as $item)
            {
                OrderItem::create([
                    'order_id' => $order->id,
                    'prod_id' => $item->prod_id,
                    'quantity' => $item->prod_qty,
                    'price' => $item->products->price,
                    'prod_color' => $item->prod_color ?? null,
                    'prod_size' => $item->prod_size ?? null
                ]);

                $prod = Product::where('id', $item->prod_id)->first();
                $prod->quantity = $prod->quantity - $item->prod_qty;
                $prod->update();
            }

            $cartItem = Cart::where('user_id',Auth::id())->get();
            Cart::destroy($cartItem);
            
            return redirect()->route('home')->with('status','Order successful');
        }catch(QueryException $e){
            dd($e->getMessage());
        }
    }
}
