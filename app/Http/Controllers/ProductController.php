<?php

namespace App\Http\Controllers;

use App\Exports\ProductsExport;
use App\Http\Requests\productRequest;
use App\Models\Brand;
use App\Models\Category;
use App\Models\Color;
use App\Models\Product;
use App\Models\Size;
use App\Models\SubCategory;
use App\Models\Tag;
use Illuminate\Database\QueryException;
use Maatwebsite\Excel\Facades\Excel;
use Illuminate\Http\Request;
use Image;
use PDF;


class ProductController extends Controller
{
    public function index()
    {
        try{
            $req='';
            $productCollection = Product::latest();
            $category = Category::latest()->get();
            if(request('search')){
                $product = $productCollection
                                ->where('name','like', '%'.request('search').'%')
                                ->orWhere('short_description','like', '%'.request('search').'%');
            }
            if(request('category')){
                $product = $productCollection
                            ->where('category_id',request('category'));
                $req=request('category');
            }
                $product = $productCollection->paginate(10);
                return view('backend.products.index',[
                'products' => $product,
                'categories' => $category,
                'req'=> $req,
            ]);
            
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function create()
    {
        $categories = Category::orderBY('name','asc')
                            ->pluck('name','id')
                            ->toArray();
        $subcategories = SubCategory::orderBY('name','asc')
                            ->pluck('name','id')
                            ->toArray();
        $tag = Tag::orderBY('title','asc')
                            ->pluck('title','id')
                            ->toArray();
        $size = Size::orderBY('title','asc')
                            ->pluck('title','id')
                            ->toArray();
        $color = Color::orderBY('name','asc')
                            ->pluck('name','id')
                            ->toArray();
        $brand = Brand::orderBY('name','asc')
                            ->pluck('name','id')
                            ->toArray();
        return view('backend.products.create',[
            'categories' => $categories,
            'subcategories' => $subcategories,
            'tags' => $tag,
            'sizes' => $size,
            'colors' => $color,
            'brands' => $brand
        ]);
    }

    public function store(productRequest $request)
    {
        try{
            $product = Product::create([
                'category_id' => $request->category_id,
                'subcategory_id' => $request->subcategory_id,
                'brand_id' => $request->brand_id,
                'name' => $request->name,
                'discount' => $request->discount,
                'price' => $request->price,
                'quantity' => $request->quantity,
                'sku' => $request->sku,
                'short_description' => $request->short_description,
                'long_description' => $request->long_description,
                'featured' => $request->our_feature ? $request->our_feature : 0,
                'image1' => request()->hasFile('image1') ? $this->uploadImage(request()->file('image1'), 'one') : null,
                'image2' => request()->hasFile('image2') ? $this->uploadImage(request()->file('image2'), 'two') : null,
                'image3' => request()->hasFile('image3') ? $this->uploadImage(request()->file('image3'), 'three') : null,
                'image4' => request()->hasFile('image4') ? $this->uploadImage(request()->file('image4'), 'four') : null,
            ]);

            $product->tags()->attach($request->tag_id);
            $product->sizes()->attach($request->size_id);
            $product->colors()->attach($request->color_id);

            return redirect()->route('products.index')->withMessage('Task was successful!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function show(Product $product)
    {
        return view('backend.products.show',[
            'product' => $product
        ]);
    }

    public function edit(Product $product)
    {
        $categories = Category::orderBY('name','asc')
                            ->pluck('name','id')
                            ->toArray();
        $subcategories = SubCategory::orderBY('name','asc')
                            ->pluck('name','id')
                            ->toArray();
        $tag = Tag::orderBY('title','asc')
                            ->pluck('title','id')
                            ->toArray();
        $size = Size::orderBY('title','asc')
                            ->pluck('title','id')
                            ->toArray();
        $color = Color::orderBY('name','asc')
                            ->pluck('name','id')
                            ->toArray();
        $brand = Brand::orderBY('name','asc')
                            ->pluck('name','id')
                            ->toArray();
        $selectedTags = $product->tags->pluck('id')->toArray();
        $selectedSizes = $product->sizes->pluck('id')->toArray();
        $selectedColors = $product->colors->pluck('id')->toArray();

        return view('backend.products.edit',[
            'product' =>$product,
            'categories' => $categories,
            'subcategories' => $subcategories,
            'tags'=>$tag,
            'sizes' => $size,
            'colors' => $color,
            'brands' => $brand,
            'selectedTags' => $selectedTags,
            'selectedSizes' => $selectedSizes,
            'selectedColors' => $selectedColors
        ]);
    }

    public function update(ProductRequest $request, Product $product)
    {
        try{
            $requestData = [
                'category_id' => $request->category_id,
                'subcategory_id' => $request->subcategory_id,
                'brand_id' => $request->brand_id,
                'name' => $request->name,
                'discount' => $request->discount,
                'price' => $request->price,
                'quantity' => $request->quantity,
                'sku' => $request->sku,
                'short_description' => $request->short_description,
                'long_description' => $request->long_description,
            ];

            $product->tags()->sync($request->tag_id);
            $product->sizes()->sync($request->size_id);
            $product->colors()->sync($request->color_id);

            if($request->hasFile('image1')){
                $this->unlinkImage($product->image1);
                $requestData['image1'] =$this->uploadImage(request()->file('image1'), 'one');
            }
            if($request->hasFile('image2')){
                $this->unlinkImage($product->image2);
                $requestData['image2'] =$this->uploadImage(request()->file('image2'), 'two');
            }
            if($request->hasFile('image3')){
                $this->unlinkImage($product->image3);
                $requestData['image3'] =$this->uploadImage(request()->file('image3'), 'three');
            }
            if($request->hasFile('image4')){
                $this->unlinkImage($product->image4);
                $requestData['image4'] =$this->uploadImage(request()->file('image4'), 'four');
            }
            $product->update($requestData);
            return redirect()->route('products.index')->withMessage('Task was successfully Update!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function destroy(Product $product)
    {
        try{
            $product->delete();
            return redirect()->route('products.index')->withMessage('Task was successfully Delete!');
        }catch(QueryException $e){
            return redirect()->back()->withInput()->withErrors($e->getMessage());
        }
    }

    public function trash()
    {
        try{
            $product = Product::onlyTrashed()->get();
            return view('backend.products.trash',[
                'products' => $product
            ]);
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function restore($id)
    {
        try{
            $product = Product::onlyTrashed()->findOrFail($id);
            $product->restore();
            return redirect()->route('products.trash')->withMessage('Task was successfully Restore!');
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function delete($id)
    {
        try{
            $product = Product::onlyTrashed()->findOrFail($id);
            $product->forceDelete();
            $product->tags()->detach();
            $product->sizes()->detach();
            $product->colors()->detach();
            return redirect()->route('products.trash')->withMessage('Task was successfully Permanent Delete!');
        }catch(QueryException $e){
            echo $e->getMessage();
        }
    }

    public function uploadImage($file, $name=null)
    {
        $fileName = $name.time().'.'.$file->getClientOriginalExtension();
        Image::make($file)->resize(600, 600)->save(storage_path().'/app/public/images/products/'.$fileName);
        return $fileName;
        
    }

    public function unlinkImage($fileName)
    {
        if(file_exists(storage_path().'/app/public/images/products/'.$fileName && !is_null($fileName))){
            unlink(storage_path().'/app/public/images/products/'.$fileName);
        }
    }

    public function excel() 
    {
        return Excel::download(new ProductsExport, 'product.xlsx');
    }

    public function pdf()
    {
        $product = Product::all();
        $pdf = PDF::loadView('backend.products.pdf', ['products' => $product]);
        return $pdf->download('products.pdf');
    }
}
