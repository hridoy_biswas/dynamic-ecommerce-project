<?php

namespace App\Http\Requests;

use Illuminate\Foundation\Http\FormRequest;

class productRequest extends FormRequest
{
    /**
     * Determine if the user is authorized to make this request.
     *
     * @return bool
     */
    public function authorize()
    {
        return true;
    }

    /**
     * Get the validation rules that apply to the request.
     *
     * @return array
     */
    public function rules()
    {
        $productId = $this->route('product')->id ?? '';
        $imageValidation1 = 'image|mimes:png,jpg';
        $imageValidation2 = 'image|mimes:png,jpg';
        if($this->isMethod('post')){
            $imageValidation1 ='required|'.$imageValidation1;
        }
        return [
            'subcategory_id' => 'required',
            // 'size_id' => 'required',
            // 'color_id' => 'required',
            // 'brand_id' => 'required',
            'name' => 'required',
            'short_description' => 'required',
            'long_description' => 'required',
            'price' => 'required',
            'quantity' => 'required',
            'sku' => 'required|unique:products,sku,'.$productId,
            'image1' => $imageValidation1,
            'image2' => $imageValidation2,
            'image3' => $imageValidation2,
            'image4' => $imageValidation2
        ];
    }
}
