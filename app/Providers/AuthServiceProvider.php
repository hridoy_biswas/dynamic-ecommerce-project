<?php

namespace App\Providers;

use App\Models\User;
use FontLib\Table\Type\post;
use Illuminate\Foundation\Support\Providers\AuthServiceProvider as ServiceProvider;
use Illuminate\Support\Facades\Gate;

class AuthServiceProvider extends ServiceProvider
{
    /**
     * The policy mappings for the application.
     *
     * @var array<class-string, class-string>
     */
    protected $policies = [
        // 'App\Models\Model' => 'App\Policies\ModelPolicy',
    ];

    /**
     * Register any authentication / authorization services.
     *
     * @return void
     */
    public function boot()
    {
        $this->registerPolicies();

        Gate::define('admin', function (User $user) {
            if($user->role_id == 1){
            return true;
            } return false;
            // return $user->id === $post->user_id;
        });
        Gate::define('editor', function (User $user) {
            if($user->role_id == 2){
            return true;
            } return false;
            // return $user->id === $post->user_id;
        });
        Gate::define('viewer', function (User $user) {
            if($user->role_id == 3){
            return true;
            } return false;
            // return $user->id === $post->user_id;
        });
    }
}
